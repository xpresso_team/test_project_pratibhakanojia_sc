"""
This is a flask test module. It list down the tests required for flask
app
"""
import os

from service-publisher.app import app as flask_app
from service-publisher.app import config_file


def test_flask_app_root():
    """
    Call flask method to check the resposne
    """

    # Flask provides a way to test your application by exposing the Werkzeug
    # test Client and handling the context locals for you.
    test_client = flask_app.test_client()

    response = test_client.get('/')
    assert response.status_code == 200
    assert b"Hello World" in response.data


def test_flask_app_404():
    """
    Call flask method to check the resposne
    """
    backup_config_file = str(config_file + ".backup")
    os.rename(config_file, backup_config_file)
    # Flask provides a way to test your application by exposing the Werkzeug
    # test Client and handling the context locals for you.
    test_client = flask_app.test_client()

    response = test_client.get('/sample')
    assert response.status_code == 404
    os.rename(backup_config_file, config_file)


def test_flask_app_no_config():
    """
    Call flask method to check the resposne
    """
    backup_config_file = str(config_file + ".backup")
    os.rename(config_file, backup_config_file)
    # Flask provides a way to test your application by exposing the Werkzeug
    # test Client and handling the context locals for you.
    test_client = flask_app.test_client()

    response = test_client.get('/')
    assert response.status_code == 200
    os.rename(backup_config_file, config_file)
